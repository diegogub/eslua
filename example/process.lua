local processor = require "src/processor"
local socket = require("socket")
local evento = require "src/eslua"
local ecli = evento.cli:new("http://localhost:6060")
local j = require("cjson"):new()


local match_all = function(state,id,type,data,replay)
    local j = require("cjson"):new()
    print(id)
    print(type)
    print(j.encode(data))
end

local collect = function(state,id,type,data,replay)
    local count = state["types"][type]
    if count == nil then
        state["types"][type] = 0
    else
        state["types"][type] = state["types"][type] + 1
    end
end

local stats = function(state,id,type,data,replay)
    local j = require("cjson"):new()

    local event_date = data.timestamp
    if event_date == nil then
        event_date = data.ts
    end

    local timeslot = os.date("%Y-%m-%d=%H", event_date)
    if type == "click" or type == "sale" then
        state["stats"] = state["stats"] or {}
        state["stats"][timeslot] = state["stats"][timeslot] or {}
        state["stats"][timeslot][type] = state["stats"][timeslot][type] or 0
        state["stats"][timeslot][type] = state["stats"][timeslot][type] + 1
    end

    print(data.timestamp)
    print(j.encode(state["stats"]))
end

local proc = processor:new(ecli,"testing"):save_snapshot()
proc:omit_failed()
proc.verbose = true

proc:add_handler("log","*",match_all)
proc:add_handler("stats","*",stats)


local state = { types = {}}
proc:run("pap_events",state,0,false)

-- print(j.encode(state))


local test_stream_changes = proc:listen("test",{},true)
local test2_stream_changes = proc:listen("test2",{},true)
while true do
    proc:check(test_stream_changes)
    proc:check(test2_stream_changes)
    socket.sleep(0.5)
end
