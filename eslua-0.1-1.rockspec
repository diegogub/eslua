package = "eslua"
version = "0.1-1"
source = {
   url = "https://gitlab.com/diegogub/eslua/-/archive/master/eslua-master.tar.gz",
   tag = "master",
}
description = {
   summary = "Evento store driver for Lua",
   detailed = [[
        This package is a client driver for the event-store Evento.
   ]],
   homepage = "",
   license = "MIT"
}
dependencies = {
   "lua >= 5.1",
   "httpclient >= 0.1-8"
}
external_dependencies = {}
build = {
   type = "builtin",
   modules = {
      eslua = "src/eslua.lua",
      ["eslua.processor"] = "src/processor.lua",
      json = "src/json.lua"
   }
}
