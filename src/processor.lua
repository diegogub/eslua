-- Event processor


-- Processor listen for events
Processor = { id = "", client = {}, handlers = {}, state_handler = state_handler, snapshot = false, save_status = false, starting_event = -1 , listen_changes = false, fail_fast = true, verbose = true}
function Processor:new(client,id)
    local self = {}
    setmetatable(self, { __index = Processor})

    self.client = client
    -- id of processor to save status
    self.id = id or error("Processor should have one id")

    return self
end

-- start reading stream from version n
function Processor:start_from(n)
    assert(type(n) == "number")
    self.starting_event = n
    return self
end

-- activate snapshot into event_store
function Processor:save_snapshot()
    self.snapshot = true
    return self
end

function Processor:listen_changes(listen)
    self.listen_changes = true
    return self
end

function Processor:omit_failed()
    self.fail_fast = false 
    return self
end

function Processor:add_handler(name,query,handler)
    assert(type(name) == "string")
    assert(type(query) == "string")
    assert(type(handler) == "function")

    table.insert(self.handlers,{ n = name, q = query, h = handler })
end

local stream_version = function(proc)
end

function Processor:match_type(type)
    local handlers = {}
    for q,handler in pairs(self.handlers) do
        local matched  = type:match(handler.q)
        if matched ~= "" then
            handlers[handler.n] = handler.h
        end
    end

    return handlers
end

-- starts running proccessor over stream
function Processor:run(stream,state,from,replay) 
    assert(type(stream) == "string")
    assert(type(replay) == "boolean")
    assert(type(state) == "table")

    local last_version = -1
    local c  ,err = self.client:read(stream,from)
    print(c,err)
    if err then
        error(err)
    end
    while c:next(false) do
        for i=1, c:count() do
            local event = c:event(i)
            if event then
                local handlers = self:match_type(event.type)
                if self.verbose then
                    print("Event with type:" .. event.type .. " matched:")
                    for q,h in pairs(handlers) do
                        print(q,h)
                    end
                end

                if #handlers == 0 and self.fail_fast then
                    error("Event with type:" .. event.type .." did not match any handler")
                end

                for q,h in pairs(handlers) do
                    local err = h(state,event.id,event.type,event.data,replay)
                    if err then
                        print(err)
                        if self.fail_fast then
                            error(err)
                        end
                    else
                        last_version = event.version
                    end
                end
            end
        end
    end

    return last_version 
end

-- Start listening changes on a stream
function Processor:listen(stream,state,verbose)
    local version ,err = self.client:version(stream)
    if err then
        version = -1
    end
    return {
        stream = stream,
        current_version = version,
        state = state,
        verbose = verbose
    }
end

-- check state of listener, if changes happened, run processor
function Processor:check(listener)
    local version = self.client:version(listener.stream)
    if listener.verbose then
        print(".Stream: ".. listener.stream .." .Last version: " .. listener.current_version .." .Current version:" .. version)
    end
    if listener.current_version < version then
        if listener.verbose then
            print("Starting running from: " ..listener.current_version + 1)
        end

        listener.current_version = self:run(listener.stream,listener.state,listener.current_version + 1, true)
    end
end

-- Example of defaul handler
-- I get a context to save data between handlers and whole event
local default_handler = function(ctx,event_id,event_type,event_data,is_replay)
end

return Processor
