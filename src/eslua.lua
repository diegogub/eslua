local _M = {}
local httpclient = require "httpclient"
local json = require "json"
local page_size = 200

EventoClient = { host = "http://localhost" , timeout = 6060 , endpoints = {}}
function EventoClient:new(host)
    local self = {}
    setmetatable(self, { __index = EventoClient })
    self.host = host or "http://localhost:6060"
    self.page_size = page_size

    --make http client
    self.cli =  httpclient.new()

    self.endpoints = {
        ping    = self.host .. "/ping",
        version = self.host .. "/stream/{s}",
        get_version = self.host .. "/stream/{s}/{v}",
        status = self.host .. "/status/{s}",
        range = self.host .. "/range/{s}/{from}/{to}",
        save = self.host .. "/stream/{s}?limited={limited}&limit={limit}&create={create}",
        exist = self.host .. "/stream/{id}",
        event = self.host .. "/event/{id}",
        truncate = self.host .. "/truncate/{s}/{left}"
    }
    return self
end

-- Event obect
EventoEvent = { id = "" , type = "", stream = "", data = {}, lock = -1, limited_stream = -1, links = {} , create = false}

function EventoEvent:new(stream,event_type,data)
    assert(type(stream) == "string")
    assert(type(event_type) == "string")
    assert(type(data) == "table")

    local self = {}
    setmetatable(self, { __index = EventoClient })
    self.stream = stream or ""
    self.type = event_type or ""
    self.data = data or {}
    self.lock = -1
    self.limited_stream = 0
    self.create = create or false
    self.links = links or {}
    return self
end

function EventoEvent:set_id(id)
    self.id = id or ""
end

function EventoEvent:lock(lock)
    self.lock = lock or -1
end

function EventoEvent:locked(limit)
    self.limited_stream = limit or -1
end

-- save store evento to eventstore
function EventoClient:save(e)
    if e == nil then
        return -1, err
    end
    local path = self.endpoints.save:gsub("{s}",e.stream)
    
    path = path:gsub("{lock}",e.lock)
    if e.limited_stream > 0  then
        path = path:gsub("{limited}","true")
        path = path:gsub("{limit}",e.limited_stream)
    else
        path = path:gsub("{limited}","false")
        path = path:gsub("{limit}",e.limited_stream )
    end

    if e.create then
        path = path:gsub("{create}","true")
    else
        path = path:gsub("{create}","false")
    end

    -- add some metadata
    e.data._t = os.time()
    local event = json.encode({ id = e.id or "", type = e.type , data = e.data , streams = e.links })
    local res = self.cli:post(path,event)

    if res.err then
        return -1, res.err
    else
        local reply = json.decode(res.body)
        return reply.version,nil
    end
end


function EventoClient:ping()
    local path = self.endpoints.ping
    local res = self.cli:get(path)
    if res.err then
        return res.err
    else
        return nil
    end
end

function EventoClient:version(stream_name) 
    local path = self.endpoints.version:gsub("{s}",stream_name)
    local res = self.cli:get(path)
    if res.err then
        return -1, res.err
    else
        local b = json.decode(res.body)
        return b.version, nil
    end
end

function EventoClient:get_version(stream_name,version) 
    local path = self.endpoints.get_version:gsub("{s}",stream_name)
    path = path:gsub("{v}",version)

    local res = self.cli:get(path)
    if res.err then
        return {}, res.err
    else
        local b = json.decode(res.body)
        return b, nil
    end
end

function EventoClient:exist(id)
    local path = self.endpoints.exist:gsub("{id}",id)
    local res = self.cli:get(path)
    if not res.err then
        return true
    else
        return false
    end
end

function EventoClient:event(id)
    local path = self.endpoints.event:gsub("{id}",id)
    local res = self.cli:get(path)
    if res.err then
        return {},res.err
    else
        return json.decode(res.body),nil
    end
end

-- range stream from one point to other
function EventoClient:range(stream_name,from,to)
    local path = self.endpoints.range:gsub("{s}",stream_name)
    path = path:gsub("{from}",tostring(from))
    path = path:gsub("{to}",tostring(to))
    local res = self.cli:get(path)
    if res.err then
       return {}, "Failed to range stream: " .. res.err
    else
        return json.decode(res.body), nil
    end
end

-- EventCursor represent a cursor into a stream of events
EventoCursor = { cli = {}, stream = "", events = {}, current = 0, page = 0 , last_version = 0}

function EventoCursor:new(cli,stream,current,last_version)
    local self = {}
    setmetatable(self, { __index = EventoCursor })
    self.cli = cli
    self.stream = stream or ""
    self.current = current or 0
    self.last_version = 0
    return self
end

function EventoCursor:count()
    return #self.events
end

function EventoCursor:event(i)
    return self.events[i] or {}
end

function EventoCursor:update() 
    local version, err = self.cli:version(self.stream)
    if err then
        return false
    end
    self.last_version = version
    return true
end

-- brings next batch of events from db
function EventoCursor:next(update) 
    if update == true then
        self:update()
    end

    if self.current > self.last_version then
        return false
    end


    local batch = self.cli:range(self.stream,self.current,self.current + self.cli.page_size)
    if not batch then
        return false
    else
        if batch.events then
            if #batch.events == 0 then
                self.events = {}
                self.current = self.current + self.cli.page_size + 1
                return true
            else
                self.events = batch.events
                self.current = self.current + self.cli.page_size + 1
                return true
            end
        else
            return false
        end
    end
end

function EventoClient:read(stream_name,start_from)
    local from = start_from or 0
    -- init cursor
    local cursor = EventoCursor:new(self,stream_name)
    cursor.current = from
    cursor.cli = self
    cursor:update()
    return cursor
end

function EventoClient:status(stream_name)
    local path = self.endpoints.status:gsub("{s}",stream_name)
    local res = self.cli:get(path)
    if res.err then
        return {}, err
    else
        local status = json.decode(res.body)
        return status, nil
    end
end


function EventoClient:replay(state,stream_name,handlers)
    local c = self:read(stream_name,0)
local ecli = evento.cli:new(nil,config.evento)
    while c:next(false) do
        for e = 1, c:count() do
            local h = handlers[c:event(e).type]
            if not h then 
                state["error"] = "cannot handle:" .. c:event(e).type
                return state, err
            end

            local state , err = h(self,state,c:event(e))
            if err then
                return state, err
            end
        end
    end

    return state, nil
end

function EventoClient:truncate(stream_name,left)
    local path = self.endpoints.truncate:gsub("{s}",stream_name)
    path = path:gsub("{left}",left)
    local res = self.cli:put(path,"{}")
    if res.err then
        return res.err
    else
        return nil
    end
end


_M = { cli = EventoClient, event = EventoEvent}

return _M 
